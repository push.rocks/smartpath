import { tap, expect, expectAsync } from '@pushrocks/tapbundle';
import * as smartpath from '../ts/index.js';

let mySmartpath: smartpath.Smartpath;
tap.test('expect create a valid instance', async () => {
  mySmartpath = new smartpath.Smartpath('/some/path/to/some.file');
  expect(mySmartpath).toBeInstanceOf(smartpath.Smartpath);
  expect(mySmartpath.pathLevelsBackwards.length === 5).toBeTrue();
});

let filePathString = './somedir/somefile.json';
let dirPathString = './somedir/anotherdir';
let dirPathString2 = './somedir/another.dir/';

tap.test('expect be true for a file path', async () => {
  expect(smartpath.check.isFile(filePathString)).toBeTrue();
});
tap.test('expect be false for a directory path', async () => {
  expect(smartpath.check.isFile(dirPathString)).toBeFalse();
  expect(smartpath.check.isFile(dirPathString2)).toBeFalse();
});

tap.test('expect be true for a directory path', async () => {
  expect(smartpath.check.isDir(dirPathString)).toBeTrue();

  expect(smartpath.check.isDir(dirPathString2)).toBeTrue();
});

tap.test('expect be false for a file path', async () => {
  expect(smartpath.check.isDir(filePathString)).toBeFalse();
});

let baseString = '/basedir';
let relativeString = 'somedir/somefile.txt';
let relativeString2 = 'anotherdir/anotherfile.txt';
let relativeArray = [relativeString, relativeString, relativeString2];
tap.test('expect make a string absolute', async () => {
  expect(smartpath.transform.toAbsolute(relativeString)).toStartWith('/');
  expect(smartpath.transform.toAbsolute(relativeString)).toEndWith(relativeString);
  expect(smartpath.transform.toAbsolute(relativeString, baseString)).toEqual(
    '/basedir/somedir/somefile.txt'
  );
});
tap.test('expect make an array of relative Strings an Array of absolute Strings', async () => {
  let absoluteArray = smartpath.transform.toAbsolute(relativeArray, baseString);
  expect(absoluteArray[2]).toStartWith('/');
  expect(absoluteArray[2]).toEndWith(relativeString2);
});

tap.test("expect return 'url' for an URL", async () => {
  expect(smartpath.get.type('https://push.rocks/some/url')).toEqual('url');
  expect(smartpath.get.type('https://push.rocks/some/url')).not.toEqual('local');
});
tap.test("expect return 'path' for a Path", async () => {
  expect(smartpath.get.type('/some/absolute/path/')).toEqual('local');
  expect(smartpath.get.type('./some/relative/path/')).not.toEqual('url');
});

tap.test('expect a absolute path for an home relative URL', async () => {
  console.log(smartpath.get.home('~/test'));
});
tap.test('expect return the home directory path when no argument is specified', async () => {
  console.log(smartpath.get.home());
});

tap.start();
