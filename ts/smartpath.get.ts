import * as plugins from './smartpath.plugins.js';
export type TPathType = 'url' | 'local';

/**
 * returns the type of the given path. Can be "url" or "local"
 */
export const type = (pathStringArg: string): TPathType => {
  const urlRegex = /http[s|\s]:\/\/.*/i;
  if (urlRegex.exec(pathStringArg)) {
    return 'url';
  } else {
    return 'local';
  }
};

/**
 * gets the dirname from import.meta.url
 */
export const dirnameFromImportMetaUrl = (importMetaUrlArg: string) => {
  return plugins.path.dirname(plugins.url.fileURLToPath(importMetaUrlArg));
};

/**
 * returns homedir as absolute path
 * @param pathArgument if a pathargument is given, ~ is being replaced with the homedir
 * @returns
 */
export const home = (pathArgument?: string) => {
  if (pathArgument) {
    return pathArgument.replace('~', plugins.os.homedir());
  } else {
    return plugins.os.homedir();
  }
};

export type TSystemArg = 'dynamic' | 'windows' | 'linux' | 'osx';

export const pathLevels = (pathArg: string, systemArg: TSystemArg = 'dynamic') => {
  let pathLevelArray: string[];
  if (systemArg === 'dynamic') {
    pathLevelArray = pathArg.split(plugins.path.sep);
  }
  return pathLevelArray;
};

export const pathLevelsBackwards = (pathArg: string, systemArg?: TSystemArg) => {
  return pathLevels(pathArg, systemArg).reverse();
};
